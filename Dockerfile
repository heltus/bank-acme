FROM python:3.7.5-buster

WORKDIR /usr/src/app

COPY requirements.txt ./requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY . ./

EXPOSE 5000

CMD [ "flask", "run", "-h", "0.0.0.0", "-p", "5000"]
