import os
import json
from pathlib import Path

from flask import Flask, request, jsonify
from flask_pymongo import PyMongo

from common import WorkFlow


app = Flask(__name__)

mongodb_client = PyMongo(
    app, 
    uri=f"mongodb://{os.environ.get('MONGODB_HOSTNAME', '127.0.0.1')}:27017/acme_bank"
)
db = mongodb_client.db


# initial user
if not db.user_account.find_one({'user_id': '105398891'}):
    app.logger.debug('Not exist, creating user for test.')
    db.user_account.insert_one({
        'user_id': '105398891', 'pin': 2090, 'balance': 0, 'is_valid': True
    })


@app.route('/', methods=['POST'])
def index():
    if 'file' in request.files:
        file = request.files['file']
        data = json.loads(file.read())
        wf = WorkFlow(db, data)
        wf.router()
        print(wf.log)
        return jsonify({'status': wf.log})

    return jsonify({'status': 'File Not Provided'})
