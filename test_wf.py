import pytest

from common import WorkFlow
from wf_exceptions import InvalidStartInputException, NotStepsException


def test_invalid_steps():
    data = {
        "steps": [],
        "trigger": {
        "params": {
            "user_id": "105398891",
            "pin": 2090
        },
        "transitions": [
            {
            "target": "validate_account",
            "condition": []
            }
        ],
        "id": "start"
        }
    }

    with pytest.raises(NotStepsException):
        assert WorkFlow(data)


def test_invalid_start():
    data = {
        "steps": [
            {
                "id": "validate_account",
                "params": {
                    "user_id": {"from_id": "start", "param_id": "user_id"},
                    "pin": {"from_id": "start", "param_id": "pin"}
                },
                "action": "validate_account",
                "transitions": [
                    {
                    "condition": [
                        {"from_id": "validate_account", "field_id": "is_valid", "operator": "eq", "value": True}
                    ],
                    "target": "account_balance"
                    }
                ]
            }
        ],
        "trigger": {}
    }
    with pytest.raises(InvalidStartInputException):
        assert WorkFlow(data)
