import json
from pathlib import Path
from time import time
from typing import Union
from functools import wraps

import pymongo

from wf_exceptions import InvalidAccountException, InvalidStartInputException, NotStepsException


def get_step_params(f):
    @wraps(f)
    def inner(*args, **kwargs):
        params = args[1]
        for i in params:
            if params[i]['from_id'] == 'start':
                _data = args[0].start()['params']
                params[i] = _data[params[i]['param_id']]

            else:
                params[i] = params[i]['value']

        return f(*args, **kwargs)
    return inner


class WorkFlow:

    def __init__(self, db: pymongo, file_data: Union[Path, dict]) -> None:
        wf = self.read_workflow(file_data)
        self.db = db
        self.log = []

    def read_workflow(self, file_data: Union[Path, dict]) -> None:
        if type(file_data) != dict:
            wf = json.loads(file_data.read_text())
        else:
            wf = file_data

        self.steps = wf.get('steps')
        self.trigger = wf.get('trigger')

        if not self.steps:
            raise NotStepsException

    def start(self) -> dict:
        if self.trigger and self.trigger.get('id') == 'start':
            return self.trigger

        res = list(filter(lambda wf: wf['id'] == 'start', self.steps))
        if res:
            return res

        raise InvalidStartInputException

    def operator(self, data, condition) -> bool:
        if condition['operator'] == 'eq' and data[condition['field_id']] == condition['value']:
            return True
        elif condition['operator'] == 'gt' and data[condition['field_id']] > condition['value']:
            return True            
        elif condition['operator'] == 'gte' and data[condition['field_id']] >= condition['value']:
            return True
        elif condition['operator'] == 'lt' and data[condition['field_id']] < condition['value']:
            return True
        elif condition['operator'] == 'lte' and data[condition['field_id']] <= condition['value']:
            return True
        return False

    def transitions(self, trans: dict, data_pre_step):
        for t in trans:
            _result = []
            for condition in t['condition']:
                _result.append(self.operator(data_pre_step, condition))
            if all(_result):
                for res in list(filter(lambda wf: wf['id'] == t['target'], self.steps)):
                    self.router(res)

    def router(self, step=None) -> None:
        start_input = step if step else self.start()
        _data = None
        if start_input.get('action'):
            _data = self.exec_func(start_input['action'], start_input['params'])
        
        self.transitions(start_input['transitions'], _data)

    def exec_func(self, action: str, params: dict) -> dict:
        module = eval('self.{}'.format(action))
        return module(params)

    @get_step_params
    def validate_account(self, params: dict) -> dict:
        user = self.db.user_account.find_one({'user_id': params['user_id'], 'pin': params['pin']})
        if not user:
            raise InvalidAccountException
        self.log.append(f"Success validate_account: {user['user_id']}")
        return user

    @get_step_params
    def get_account_balance(self, params: dict) -> dict:
        balance = self.db.user_account.find_one({'user_id': params['user_id']}, {'balance': True})
        self.log.append(f"Success account_balance: {balance['balance']}")
        return balance
    
    @get_step_params
    def deposit_money(self, params: dict) -> dict:
        self.db.user_account.update_one(
            {'user_id': params['user_id']}, 
            {'$set': {'balance': params['money']}}
        )
        self.log.append(f"Success deposit_money: {params['money']}")
        return {}

    def get_trm(self) -> int:
        return 4000

    @get_step_params
    def withdraw_in_dollars(self, params: dict) -> dict:
        cop = (params['money'] * self.get_trm()) * -1
        self.db.user_account.update_one(
            {'user_id': params['user_id']}, 
            {'$inc': {'balance': cop}}
        )
        self.log.append(f"Success withdraw_in_dollars: {params['money']} (COP: {cop})")
        return {}
    

    @get_step_params
    def withdraw_in_cop(self, params: dict) -> dict:
        self.db.user_account.update_one(
            {'user_id': params['user_id']}, 
            {'$inc': {'balance': params['money'] * -1}}
        )
        self.log.append(f"Success withdraw_in_cop: {params['money']}")
        return {}
