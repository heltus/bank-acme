class NotStepsException(Exception):
    pass


class InvalidStartInputException(Exception):
    pass


class InvalidAccountException(Exception):
    pass

